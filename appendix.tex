
\section{Derivation of the formula for the width of the spectral gap}
\label{sec:deriv_gap}

Analysis of band diagrams $\omega_{\alpha}(\vec{k})$ of the Hamiltonian $\hat{{\cal H}}(\vec{k})$ given by Eqs.\ (\ref{hamk})--(\ref{h22}) shows that the width of the gap between the second and third bands is determined by the values of $\omega_2$ and $\omega_3$ at very specific points of BZ: $\Gamma$, $K$ and $K$' points. At these points, $\hat{{\cal H}}(\vec{k})$ takes quite simple forms facilitating its diagonalization. In particular, at $K$ point we have
\begin{align}
\hat{{\cal H}}(\vec{k}_K) = c_0 \mathds{1}_4 + 2
\left(\begin{array}{cccc}
\Delta_{AB} + \Delta_{\textbf{B}} & 0 & 0 & \Omega \\
0 & \Delta_{AB} - \Delta_{\textbf{B}} & 0 & 0 \\
0 & 0 & -\Delta_{AB} + \Delta_{\textbf{B}} & 0 \\
\Omega & 0 & 0 & -\Delta_{AB} - \Delta_{\textbf{B}}
\end{array}\right)
\end{align}
where
\begin{eqnarray}
c_0 &=& i + \sum\limits_{\vec{r}_n \ne 0} G_{++}(\vec{r}_n) e^{i \vec{k}_K \cdot \vec{r}_n}
= i + \frac{1}{\mathcal{A}}\sum_{\vec{g}_m} g_{++}(\vec{g}_m - \vec{k}_K)  - G_{++}(\vec{r} = 0)
\nonumber \\
&=&
\frac{1}{\mathcal{A}}\sum_{\vec{g}_m} g_{++}(\textbf{g}_m - \vec{k}_K)  - \text{Re}G_{++}(\vec{r} = 0)
\label{appc0}
\\
\Omega &=& \sum\limits_{\vec{r}_n} G_{+-}(\vec{r}_n + \vec{a}_1) e^{i \vec{k}_K \cdot \vec{r}_n}
= \frac{1}{\mathcal{A}}\sum_{\vec{g}_m} g_{+-}
(\vec{g}_m - \vec{k}_K) e^{i\vec{a}_1\cdot(\vec{g}_m - \vec{k}_K)}
\label{appomega}
\end{eqnarray}
are functions of $k_0 a$ and $\vec{k}_K = \{ K, 0 \}$ is the value of $\vec{k} = \{k_x, k_y \}$ at the $K$ point of BZ with $K = 4\pi/3\sqrt{3} a$. For $k_0 a \ll1$, we find $|\Omega| \gg |c_0|$.
The four eigenvalues of $\hat{{\cal H}}(\vec{k}_K)$ are real:
\begin{eqnarray}
\Lambda^K &=& c_0 \pm 2 \sqrt{(\Delta_{\textbf{B}}-\Delta_{AB})^2 + \Omega^2/4}
\simeq c_0 \pm \Omega
\label{lambda14}
\end{eqnarray}
and
\begin{eqnarray}
\Lambda^K &=& c_0 \pm  2 (\Delta_{AB} + \Delta_{\textbf{B}})
\label{lambda23}
\end{eqnarray}
The result at $K'$ point $\vec{k}_K' = -\vec{k}_K$ is obtained by changing the sign in front of $\Delta_{\textbf{B}}$ in the above expressions.

At the $\Gamma$ point $\vec{k}_{\Gamma} = 0$ we find
\begin{align}
\hat{{\cal H}}(0) &=& 
(c_1 + i c_3) \mathds{1}_4 +
%% \nonumber \\
%% &+&
2 \left(\begin{array}{cccc}
\Delta_{AB} + \Delta_{\textbf{B}} & 0 & (c_2 + ic_3)/2 & 0 \\
0 & \Delta_{AB} - \Delta_{\textbf{B}} & 0 & (c_2 + ic_3)/2\\
(c_2 + ic_3)/2 & 0 & - \Delta_{AB}+ \Delta_{\textbf{B}} & 0 \\
0 & (c_2+ ic_3)/2 & 0 & - \Delta_{AB} - \Delta_{\textbf{B}}
\end{array}\right)
\end{align}
where $c_1$, $c_2$ and $c_3$ are real-valued functions of $k_0 a$ and are defined by the following relations:
\begin{eqnarray}
c_1 + i c_3 &=& i + \sum\limits_{\vec{r}_n \ne 0} G_{++}(\vec{r}_n)
= i + \frac{1}{\mathcal{A}}\sum_{\vec{g}_m} g_{++}(\vec{g}_m)  - G_{++}(\vec{r} = 0)
\nonumber \\
&=&
\frac{1}{\mathcal{A}}\sum_{\textbf{g}_m} g_{++}(\textbf{g}_m)  - \text{Re}G_{++}(\vec{r} = 0)
\label{appc1c3}
\\
c_2 + i c_3 &=& \sum\limits_{\vec{r}_n} G_{++}(\vec{r}_n + \vec{a}_1) 
= \frac{1}{\mathcal{A}}\sum_{\vec{g}_m} g_{++}
(\vec{g}_m) e^{i\vec{a}_1\cdot \vec{g}_m}
\label{appc2c3}
\end{eqnarray}
The four complex eigenvalues of  $\hat{{\cal H}}(0)$ are given by
\begin{eqnarray}
\Lambda_{\Gamma} = c_1 + ic_3 \pm 2\Delta_{\textbf{B}} \pm 2\sqrt{\Delta_{AB}^2 + (c_2 + i c_3)^2/4}
\label{lambda0}
\end{eqnarray}
with the four possible combinations of $\pm$ signs.

Further analysis reduces to following the eigenvalues $\Lambda_K$, $\Lambda_{K'}$ and the real part of $\Lambda_{\Gamma}$ as $\Delta_{\vec{B}}$ varies at fixed $k_0 a$ and $\Delta_{AB}$. At each value of $\Delta_{\vec{B}}$, we sort $\Lambda_{K}$ and $\text{Re}\Lambda_{\Gamma}$ in descending order and find the width of the gap between the second and third bands as
\begin{eqnarray}
\Delta_{\text{gap}} &=& \frac12 \min\left\{ 
\Lambda_K^{(2)}-\Lambda_K^{(3)},
\Lambda_K^{(2)}-\text{Re}\Lambda_{\Gamma}^{(3)},
\text{Re}\Lambda_{\Gamma}^{(2)}-\Lambda_K^{(3)},
\text{Re}\Lambda_{\Gamma}^{(3)}-\text{Re}\Lambda_{\Gamma}^{(2)},
\right.
\nonumber \\
&& \left. \Lambda_{K'}^{(2)}-\Lambda_{K'}^{(3)},
\Lambda_{K'}^{(2)}-\text{Re}\Lambda_{\Gamma}^{(3)},
\text{Re}\Lambda_{\Gamma}^{(2)}-\Lambda_{K'}^{(3)}
\right\}
\label{defgap}
\end{eqnarray}
As a result of such an analysis, we find that the width of the spectral gap depends only on the absolute values of $\Delta_{\vec{B}}$ and $\Delta_{AB}$ and identify three threshold values of $|\Delta_{\vec{B}}|$ at which the functional dependence of $\Delta_{\text{gap}}$ on parameters changes because a different term starts to control the minimum in Eq.\ (\ref{defgap}): 
\begin{eqnarray}
\Delta_{\vec{B}}^{(1)} &=& \frac{1}{4} \left| c_0-c_1+S+2|\Delta_{AB}| \right|
\label{d1}
\\
\Delta_{\vec{B}}^{(2)} &=& \frac{1}{4}\left| c_0-c_1-S-2|\Delta_{AB}| \right|
\label{d2}
\\
\Delta_{\vec{B}}^{(3)} &=& \frac{S}{2}
\label{d3}
\label{threshold}
\end{eqnarray}
where
\begin{eqnarray}
S = 2 \text{Re}\sqrt{\Delta_{AB}^2 + \frac{1}{4}(c_2 + i c_3)^2}
\label{sdef}
\end{eqnarray}
The result for the width of the gap is 
\begin{eqnarray}
\Delta_{\text{gap}} =  
\begin{cases}
 2 || \Delta_{\textbf{B}} | - | \Delta_{AB}||,  &|\Delta_{\textbf{B}}| < \Delta_{\mathbf{B}}^{(1)} \\
\left| \frac12(c_0 - c_1 + S) - | \Delta_{AB}| \right|,  &\Delta_{\mathbf{B}}^{(1)} < |\Delta_{\textbf{B}}| <\Delta_{\mathbf{B}}^{(2)} \\
S -  2|\Delta_{\vec{B}}|, &\Delta_{\mathbf{B}}^{(2)} < |\Delta_{\textbf{B}}| < \Delta_{\mathbf{B}}^{(3)} \\
0, &|\Delta_{\textbf{B}}| > \Delta_{\mathbf{B}}^{(3)} 
\end{cases}
\label{appgap}
\end{eqnarray}
Equations (\ref{appgap}) can also be rewritten in terms of $\Delta_{\vec{B}}^{(n)}$ only, without using the parameters $c_n$. This is done in Eq.\ (\ref{eqn:wgap}) of the main text.

%% If we add the plates then the imaginary coefficient $c_3$ vanishes and we get:


%% \section{\blue{REMOVE ?}Fitting coefficients of $M(\textbf{k})$ at symmetry points}
%% \label{sec:coeffMk}
%% Here we give the values obtained after fitting the width of the gap
%% for several values of \(k_0a\).

%% \begin{itemize}
%% \item \(c_0 = C_{0,1}/(k_0a/2\pi)^{-1} + C_{0,2}/(k_0a/2\pi)^{-2} +
%%   C_{0,3}/(k_0a/2\pi)^{-3}\) with \(C_{0,1} = -0.1489803\), \(C_{0,2} =
%%   -0.0002726\) and \(C_{0,3} = -0.00135505\)
%% \item \(c_1 = C_{1,1}/(k_0a/2\pi)^{-1} + C_{1,2}/(k_0a/2\pi)^{-2} +
%%   C_{1,3}/(k_0a/2\pi)^{-3}\) with \(C_{1,1} = -0.43169\), \(C_{1,2} =
%%   - 1.37252819e-04\) and \(C_{1,3} = 6.42156198e-03\)
%% \item \(c_2 = C_{2,1}/(k_0a/2\pi)^{-1} + C_{2,2}/(k_0a/2\pi)^{-2} +
%%   C_{2,3}/(k_0a/2\pi)^{-3}\) with \(C_{2,1} = -0.162950412\), \(C_{2,2} =
%%   2.31735224e-05\) and \(C_{2,3} = 0.0134708817\)
%% \item \(c_3 =  C_{3,2}/(k_0a/2\pi)^{-2}\) with \(C_{3,2} = 0.09188391\)
%% \end{itemize}
