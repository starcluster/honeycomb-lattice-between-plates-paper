\contentsline {section}{\numberline {}Abstract}{1}{}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Introduction}{1}{}
\contentsline {section}{\numberline {II}Honeycomb atomic lattice in the free space}{2}{}
\contentsline {subsection}{\numberline {A}The model}{2}{}
\contentsline {subsection}{\numberline {B}Band diagram of a honeycomb lattice in the free space}{4}{}
\contentsline {subsection}{\numberline {C}Width of the band gap}{6}{}
\contentsline {subsection}{\numberline {D}Topological properties of the band structure}{8}{}
\contentsline {section}{\numberline {III}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{10}{}
\contentsline {subsection}{\numberline {A}Green's function in a Fabry-P\'{e}rot cavity}{10}{}
\contentsline {subsection}{\numberline {B}Atom in a Fabry-P\'{e}rot cavity}{12}{}
\contentsline {subsection}{\numberline {C}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{13}{}
\contentsline {subsection}{\numberline {D}Topological properties of the band structure}{14}{}
\contentsline {section}{\numberline {IV}Conclusion}{15}{}
\contentsline {paragraph}{\numberline {a}Funding information}{17}{}
\contentsline {paragraph}{\numberline {b}Source code}{17}{}
\appendix 
\contentsline {section}{\numberline {A}Derivation of the formula for the width of the spectral gap}{17}{}
\contentsline {section}{\numberline {}References}{18}{}
